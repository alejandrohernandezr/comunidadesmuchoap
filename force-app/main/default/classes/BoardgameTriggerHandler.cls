/**
 * @description       : 
 * @last modified on  : 15-11-2021
 * @last modified by  : Alejandro Hernández
**/
public with sharing class BoardgameTriggerHandler {
    public BoardgameTriggerHandler() {
        
    }

    public static void associateProductWithNewlyCreatedBoardgame(Boardgame__c bg, Set<String> alreadyExistingProducts, List<Product2> newProducts) {
        Product2 product = new Product2();
        String boardgameBGGId = bg.BGG_id__c;

        if (!alreadyExistingProducts.contains(boardgameBGGId)) {
            product.Name = bg.Name;
            product.ProductCode = 'BG'+boardgameBGGId;
            product.IsActive = true;
            product.Boardgame__c = bg.Id;

            alreadyExistingProducts.add(boardgameBGGId);
            newProducts.add(product);
        }
    }

    @Future(callout=true)
    public static void test1() {
        System.debug('######'+Trigger.new.size());
        for (Boardgame__c bg : (List<Boardgame__c>) Trigger.new) {
            System.debug(bg.Name);
        }
    }

    // @Future(callout=true)
    // public static void retrieveInfoFromBGGApi(Map<String, String> boardgameIdsAndSearchQueries) {
    //     String queryFormed;
    //     Http http = new Http();
    //     HttpRequest req = new HttpRequest();
    //     String queryFormed;
    //     String apiUrl =  'https://boardgamegeek.com/xmlapi2/search/?query="';
    //     String gameName = '';
    //     String bggId;
        
    //     for (Integer i = 0; i < boardgameIdsAndSearchQueries.size(); i++) {
    //     // for (String query : boardgameIdsAndSearchQueries.values()) {
    //         Boardgame__c boardgame = new Boardgame__c();

    //         boardgame.Id = boardgameIdsAndSearchQueries.get(i);
    //         queryFormed = query.replaceAll(' ', '+');

    //         req.setEndpoint(apiUrl);
    //         req.setMethod('GET');
    //         HttpResponse resp = http.send(req);
    //         System.debug(resp.getBody());
    //         Dom.Document doc = resp.getBodyDocument();
    //         Dom.XmlNode games = doc.getRootElement();
    //         System.debug(doc);
    //         for (Dom.XmlNode game : games.getChildElements()) {
    //             gameName = game.getChildElement('name', null).getAttribute('value', null);
    
    //             if (gameName == query) {
    //                 bggId = game.getAttribute('id', null);
    
    //                 break;
    //             }
    //         }

    //         if (bggId != null) {
    //             boardgame.BGG_id__c = bggId;
    //             apiUrl = 'https://boardgamegeek.com/xmlapi2/thing?id='+bggId+'&stats=1';
    //             req.setEndpoint(apiUrl);
    //             resp = http.send(req);
    //             doc = resp.getBodyDocument();
    //             games = doc.getRootElement();
    
    //             for (Dom.XmlNode game : games.getChildElements()) {
    //                 boardgame.URL_Imagen__c = game.getChildElement('image', null).getText();
    //                 boardgame.Nota__c = Decimal.valueOf(game.getChildElement('statistics', null)
    //                                                 .getChildElement('ratings', null)
    //                                                 .getChildElement('average', null).getAttributeValue('value', null));
    //             }
    
    //             boardgame.Precio__c = Integer.valueof((Math.random() * 90) + 10);
    
    //             update boardgame;
    //         } else {
    //             boardgame.addError('Couldn\t find any game with that name. Please try again.');
    //         }
    //     }

    // }
}