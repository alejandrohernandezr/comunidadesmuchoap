/**
 * @description       : 
 * @last modified on  : 15-11-2021
 * @last modified by  : Alejandro Hernández
**/
public with sharing class AccountController {

    @AuraEnabled
    public static List<Account> getAccounts() {
        try {
            return [SELECT Id, Name FROM Account];
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static List<Account> buscarTienda(String query) {
        try {
            String queryFilter = '%'+query+'%';
            return [SELECT Id, Name FROM Account WHERE Name LIKE :queryFilter];
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
}
