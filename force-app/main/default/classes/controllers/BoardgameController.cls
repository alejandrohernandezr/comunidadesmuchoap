/**
 * @description       : 
 * @last modified on  : 12-11-2021
 * @last modified by  : Alejandro Hernández
**/
public with sharing class BoardgameController {

    @AuraEnabled
    public static List<Boardgame__c> getAllBoardgames() {
        try {
            return [SELECT Id, Name, BGG_id__c, Categoria__c, Editorial__c, Calificacion__c, Nota__c, Precio__c, Subcategoria__c, Product__c, URL_Imagen__c FROM Boardgame__c ORDER BY Name DESC];
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
}
