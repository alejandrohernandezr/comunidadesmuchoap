/**
 * @description       : 
 * @last modified on  : 10-18-2021
 * @last modified by  : Alejandro Hernández
**/
trigger ProductTrigger on Product2 (after insert) {
    Map<Id, Boardgame__c> boardgames = new Map<Id, Boardgame__c>();
    List<Boardgame__c> toUpdate = new List<Boardgame__c>();
    List<PricebookEntry> newPricebookEntries = new List<PricebookEntry>();
    Pricebook2 pricebook; 

    /*
        _         _ _     _          __             
    | |__ _  _| | |__ | |__  ___ / _|___ _ _ ___ 
    | '_ \ || | | / / | '_ \/ -_)  _/ _ \ '_/ -_)
    |_.__/\_,_|_|_\_\ |_.__/\___|_| \___/_| \___|  
    */        
    if(Trigger.isBefore) {
        if(Trigger.isInsert) {}
        if(Trigger.isUpdate) {}
    }

    if(Trigger.isAfter) {
        if(Trigger.isInsert) {
            pricebook = [SELECT Id FROM Pricebook2 WHERE IsStandard = true AND IsActive = true LIMIT 1];
            
            Set<String> boardgameIds = new Set<String>();
            for (Product2 p : Trigger.new) {
                boardgameIds.add(p.Boardgame__c);
            }

            for (Boardgame__c bg : [SELECT Id FROM Boardgame__c WHERE Product__c = null AND Id IN :boardgameIds]) {
                boardgames.put(bg.Id, bg);
            }
        }
        if(Trigger.isUpdate) {}
    }
        

    /*                         _           _   _             
            _ _ ___ __ ___ _ _ __| |  __ _ __| |_(_)___ _ _  ___
        | '_/ -_) _/ _ \ '_/ _` | / _` / _|  _| / _ \ ' \(_-<
        |_| \___\__\___/_| \__,_| \__,_\__|\__|_\___/_||_/__/
    */
    for(Product2 p : (List<Product2>) Trigger.new) {
        if(Trigger.isBefore) {
            if(Trigger.isInsert) {}
            if(Trigger.isUpdate) {}
        }

        if(Trigger.isAfter) {
            if(Trigger.isInsert) {
                ProductTriggerHandler.populateProductFieldFromBoardgameRecord(p, boardgames, toUpdate);
                ProductTriggerHandler.createPricebookEntryForNewProducts(Trigger.new, pricebook.Id, newPricebookEntries);
            }

            if(Trigger.isUpdate) {}
        }
    }


    /*
            _         _ _           __ _           
        | |__ _  _| | |__  __ _ / _| |_ ___ _ _ 
        | '_ \ || | | / / / _` |  _|  _/ -_) '_|
        |_.__/\_,_|_|_\_\ \__,_|_|  \__\___|_|  
    */
    
    if(Trigger.isBefore) {
        if(Trigger.isInsert) {}
        if(Trigger.isUpdate) {}
    }

    if(Trigger.isAfter) {
        if(Trigger.isInsert) {
            update toUpdate;
            insert newPricebookEntries;
        }

        if(Trigger.isUpdate) {}
    }
}