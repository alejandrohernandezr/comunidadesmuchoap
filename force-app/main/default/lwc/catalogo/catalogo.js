/**
 * @description       : 
 * @last modified on  : 15-11-2021
 * @last modified by  : Alejandro Hernández
**/
import { LightningElement, track, api } from 'lwc';

export default class Catalogo extends LightningElement {
    carritoItemCount = 0
    @track idCliente
    @track tiendaSeleccionada = false

    updateCarritoItemCountJs(event) {
        this.carritoItemCount = event.detail
        console.log(this.carritoItemCount)
    }

    updateTiendaSeleccionadaJs(event) {
        this.idTienda = event.detail
        this.tiendaSeleccionada = true
    }
}