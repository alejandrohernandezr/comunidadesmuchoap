/**
 * @description       : 
 * @last modified on  : 15-11-2021
 * @last modified by  : Alejandro Hernández
**/
import { LightningElement, track } from 'lwc';
import getAllBoardgames from '@salesforce/apex/BoardgameController.getAllBoardgames'

export default class ListaJuegos extends LightningElement {
    @track listaJuegos = []
    @track hayJuegos = false

    connectedCallback() {
        this.getAllBoardgamesJs()
    }

    getAllBoardgamesJs() {
        getAllBoardgames()
        .then( res => {
            if (res != null) {
                this.listaJuegos = res
                this.hayJuegos = true
            }
        }).catch(error => console.error(error))
    }

    addProductToCartJs() {
        this.dispatchEvent(new CustomEvent('carritoupdated', { detail: this.carrito.length }))
    }

}