/**
 * @description       : 
 * @last modified on  : 16-11-2021
 * @last modified by  : Alejandro Hernández
**/
import { LightningElement, track } from 'lwc';
import buscarTienda from '@salesforce/apex/AccountController.buscarTienda'
import getAccounts from '@salesforce/apex/AccountController.getAccounts'

export default class SelectorTienda extends LightningElement {
    @track tiendasBd = []
    @track hayResultados = true
    @track tiendasFiltradas = []
    @track searchQuery

    connectedCallback() {
        this.getAccountsJs();
    }

    buscarTiendaJs(event) {
        this.tiendasFiltradas = []

        this.tiendasBd.map(val => { 
            if ((val.Name.toLowerCase()).includes(this.searchQuery)) {
            this.tiendasFiltradas.push(val)
            }
        })
    }

    seleccionarTiendaJs(event) {
        this.dispatchEvent(new CustomEvent('tiendaseleccionada', { detail: this.carrito.length }))
        console.log(event.target.dataset.item)
        sessionStorage.set('idTienda', event.target.dataset.item)
    }

    getAccountsJs() {
        getAccounts()
        .then( res => {
            if (res != null) {
                this.tiendasFiltradas = res
                this.tiendasBd = res
                this.hayResultados = true
            } else {
                this.tiendasBd = null
                this.hayResultados = false
            }
        }).catch(error => console.error(error))
    }

    actualizarValorBusquedaJs(event) {
        this.searchQuery = event.target.value
    }
}
