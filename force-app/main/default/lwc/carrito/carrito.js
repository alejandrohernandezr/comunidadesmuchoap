/**
 * @description       : 
 * @last modified on  : 15-11-2021
 * @last modified by  : Alejandro Hernández
**/
import { LightningElement, track, api } from 'lwc';

export default class Carrito extends LightningElement {
    @api carritoitemcount = 0
    @track clienteSeleccionado = true


}